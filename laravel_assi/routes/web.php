<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', 'LoginController@userloginForm');
Route::get('/admin', 'LoginController@loginForm');
Route::post('/admin_login', 'LoginController@loginAdmin');
Route::get('/logout_Admin', 'LoginController@logoutAdmin');

Route::group(['middleware' => 'LoginCheck'],function(){

	Route::get('/dashboard', 'DashboardController@dashboard');
	Route::get('/add_user', 'AddUserController@addUserForm');
	Route::get('/status_change/{id}', 'UpdateUserController@statusChange');
	Route::get('/delete_user/{id}', 'UpdateUserController@deleteUser');
	

	Route::post('/add_user_route', 'AddUserController@addUserToDb');
	Route::post('/update_user', 'UpdateUserController@updateUser');
});


Auth::routes();
Route::group(['middleware' => 'auth'],function(){

	Route::post('/login_user', 'LoginController@loginUser');
	Route::get('/logout_user', 'LoginController@logoutUser');
	Route::get('/Userdashboard', 'DashboardController@userDashboard');
	Route::get('/updateYourself', 'UpdateUserController@updateProfileForm');

	Route::post('/update_process', 'UpdateUserController@updateUserSelf');

});

Route::get('/', 'HomeController@index')->name('auth');
