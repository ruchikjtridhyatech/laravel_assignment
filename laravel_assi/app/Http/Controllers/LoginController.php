<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Admin;
use Auth;
use DB;
use Session;


class LoginController extends Controller
{
	// redirect to Login Form
    public function loginForm()
    {
    	return view('admin.login');
    }
    public function userloginForm()
    {
    	return view('user.login');
    }

    //Admin Login Function
    public function loginAdmin(Request $adminlogin)
    {
    	$adminModel = new Admin();
      $username = $adminlogin->email;
      $password = $adminlogin->Password;
      $data = $adminModel::where('email',$username)->where('password', $password)->first();
      
	      if($data != '')
	      {
	        $adminlogin->Session()->put('adminname',$username);
	        $adminlogin->Session()->put('AdminStatus',true);
	        return redirect("/dashboard");
	      }
	      else {
	          return redirect("/admin");
	      }
      
    }
    //Logout Admin
    public function logoutAdmin(Request $adminlogin)
    {

      $adminlogin->Session()->forget('adminnm');
      $adminlogin->Session()->forget('AdminStatus');
      return redirect("/");
    }
 
}
