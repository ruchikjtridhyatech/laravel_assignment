<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class DashboardController extends Controller
{
    public function dashboard()
    {
    	$user = new User();
    	$data = $user::all();
    	return view('admin.index',['data'=>$data]);
    }
    public function userDashboard(Request $request)
    {
        $user = Auth::user();
        return view('user.index',['data'=>$user]);
    }
}
