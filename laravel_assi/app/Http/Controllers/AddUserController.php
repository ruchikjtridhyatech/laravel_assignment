<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Validator;

class AddUserController extends Controller
{
    public function addUserForm()
    {
     return view('admin.addUserForm');
    }

    public function addUserToDb(Request $data)
    {

    	$users = new User();

    	$validator=Validator::make($data->all(),[
            'name' => 'required|max:30|min:3',
            'email' => 'required|email|unique:users,email',
            'Password' => 'required|confirmed|min:6',
            'gender' => 'required',
            'image' => 'required',
            'status' => 'required',
            'phone' => 'required|digits:10',
        ]);

     if($validator->fails())
     {
     	
       return redirect('/add_user')->withErrors($validator)->withInput();
     }


     $users->name = $data->name;
     $users->phone = $data->phone;
     $users->gender = $data->gender;
     $users->email = $data->email;

     //file move upload
     $file = $data->file('image');
     $destinationPath = 'img/user_image';
     $file->move($destinationPath,$file->getClientOriginalName());
     // Over file move upload

     $users->image = $file->getClientOriginalName();
     $users->password = \Hash::make($data->Password);
     $users->status = $data->status;

        $users->save();
     return redirect('/dashboard');
    }
}
