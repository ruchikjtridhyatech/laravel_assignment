<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\User;
use Validator;
use Session;

class UpdateUserController extends Controller
{
	//User Update Function
    public function updateUser(Request $req)
    {

    	$id = $req->id; 
    	$data = new User();
    	$model = $data::find($id);

    	//update Data
    	$model->name = $req->name;
    	$model->phone = $req->phone;
    	$model->gender = $req->gender;
    	$model->email = $req->email;
    	//file move upload
    	if($req->has('userImage'))
    	{
	     $file = $req->file('userImage');
	     $destinationPath = 'img/user_image';
	     $file->move($destinationPath,$file->getClientOriginalName());
     	$model->image = $file->getClientOriginalName();
	 	}
	     // Over file move upload

    	 //Save Method
    	 $model->save();
    	return redirect('/dashboard');
    }
    // Status Changing function
    public function statusChange(Request $req)
    {
    	$id= $req->id;
    	$data = new User();
    	$model = $data::find($id);
    	if($model->status == 1)
    	{
    		$model->status = 0;
    		$model->save();
    		return redirect('/dashboard');
    	}
    	else
    	{
    		$model->status = 1;
    		$model->save();
    		return redirect('/dashboard');
    	}
    }
    //Delete User Function
    public function deleteUser(Request $req)
    {
    	$id= $req->id;
    	$user = new User();
        $data =$user::find($id);
        //delete image
    	$image_path =  "img/user_image/".$data['image'];

        if(File::exists($image_path)) {
            File::delete($image_path);
            
        }
       

    	$data::where('id', $id)->delete();
    	return redirect('/dashboard');
    }

    public function updateProfileForm()
    {
        $user = Auth::user();
        return view('user.updateProfile',['data'=>$user]);
    }
    public function updateUserSelf(Request $req)
    {
        $user = Auth::user();
        $id =Auth::user('id'); 
        
        $data = new User();
        $model = $data::find($id->id);

        //update Data
        $model->name = $req->name;
        $model->phone = $req->phone;
        $model->gender = $req->gender;

        //file move upload
        if($req->has('image'))
        {
         $file = $req->file('image');
         $destinationPath = 'img/user_image';
         $file->move($destinationPath,$file->getClientOriginalName());
        $model->image = $file->getClientOriginalName();
        }
         // Over file move upload

         //Save Method
         $model->save();
        return redirect('/Userdashboard');
    }

}
