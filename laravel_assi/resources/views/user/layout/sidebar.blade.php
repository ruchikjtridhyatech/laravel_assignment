<div class="sidebar" data-color="orange">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
      -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          HI
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          EveryOne
        </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="/Userdashboard">
              <i class="now-ui-icons design_app"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="/updateYourself">
              <i class="now-ui-icons users_single-02"></i>
              <p>Update Profile</p>
            </a>
          </li>
        </ul>
      </div>
    </div>