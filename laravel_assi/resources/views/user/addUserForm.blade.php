@extends('user.layout.master')
@section('title','Add User')
@section('activeclas','class="active"')
@section('section')
<center>
<h1 class="text-dark">Add User</h1>
{{ Form::open(array('url' => '/add_user_route','files'=>'true')) }}
	<table cellpadding="10" class="w-50 ">
	<tr>
    	<td>{{ Form::label('Name :','',array('class' => 'font-weight-bold')) }}</td>
		<td>{{ Form::text('name', $value=old('phone'),array('class' => 'form-control-plaintext border border-danger')) }}
			@if(count($errors) > 0)
                @foreach($errors->get('name') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
		</td>
	</tr>
	<tr>
    	<td>{{ Form::label('Phone Number :','',array('class' => 'font-weight-bold')) }}</td>
		<td>{{ Form::number('phone', $value=old('phone') ,array('class' => 'form-control-plaintext border border-danger')) }}
		@if(count($errors) > 0)
                @foreach($errors->get('phone') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif</td>
	</tr>
	<tr>
    	<td>{{ Form::label('Gender :','',array('class' => 'font-weight-bold')) }}</td>
		<td>{{ Form::label('Male','',array('class' => 'font-weight-bold text-primary')) }} 
			{{ Form::radio('gender', 'male') }}
			&nbsp;&nbsp;
  			{{ Form::label('Female','',array('class' => 'font-weight-bold text-primary')) }}
  			{{ Form::radio('gender', 'female') }}

  			@if(count($errors) > 0)
                @foreach($errors->get('gender') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
  		</td>
	</tr>
	<tr>
    	<td>{{ Form::label('Profile Photo :','',array('class' => 'font-weight-bold')) }}</td>
		<td>{{ Form::file('image')  }}
			@if(count($errors) > 0)
                @foreach($errors->get('image') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
		</td>
	</tr>
	<tr>
    	<td>{{ Form::label('Email :','',array('class' => 'font-weight-bold')) }}</td>
		<td>{{ Form::email('email', $value=old('email'),array('class' => 'form-control-plaintext border border-danger')) }}
		@if(count($errors) > 0)
                @foreach($errors->get('email') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif</td>
	</tr>
	<tr>
    	<td>{{ Form::label('Status :','',array('class' => 'font-weight-bold')) }}</td>
		<td>{{ Form::label('Active','',array('class' => 'font-weight-bold text-primary')) }}
			{{ Form::radio('status', '1' ) }}
			&nbsp;&nbsp;
  			{{ Form::label('inactive','',array('class' => 'font-weight-bold text-primary')) }}
  			{{ Form::radio('status', '0' ) }}

  			@if(count($errors) > 0)
                @foreach($errors->get('status') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
  		</td>
	</tr>
	<tr>
		<td>{{ Form::label('Password :','',array('class' => 'font-weight-bold ')) }}</td>
		<td>{{Form::password("Password", 
             [
                "class" => "form-control-plaintext border border-danger",
                "placeholder" => "Your Password",
             ])
			}}
			@if(count($errors) > 0)
                @foreach($errors->get('Password') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
		</td>
	</tr>
	<tr>
		<td>{{ Form::label('Confirm xPassword :','',array('class' => 'font-weight-bold ')) }}</td>
		<td>{{Form::password("Password_confirmation", 
             [
                "class" => "form-control-plaintext border border-danger",
                "placeholder" => "Confirm Password",
             ])
			}}
		</td>
	</tr>
	<tr>
		<td colspan="2" class="text-center">{{ Form::submit('Submit',array('class' => 'btn btn-primary .text-light')) }}</td>
	</tr>
	</table>
	{{ Form::close() }}
</center>
@endsection