@extends('admin.layout.master')
@section('title','Add User')
@section('activeclas','class="active"')
@section('section')
<body class="user-profile">
<div class="content">
    <div class="row">
    <div class="col-md-8">
    	<div class="card">
	    <div class="card-header">
	        <h5 class="title">Add User</h5>
	    </div>
        <div class="card-body">
		{{ Form::open(array('url' => '/add_user_route','files'=>'true')) }}
            
            <div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
	    	{{ Form::label('Name :','',array('class' => 'font-weight-bold')) }}
			{{ Form::text('name', $value=old('name'),array('class' => 'form-control','placeholder' => 'Full Name')) }}
			@if(count($errors) > 0)
                @foreach($errors->get('name') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
        	</div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
			{{ Form::label('Phone Number :','',array('class' => 'font-weight-bold')) }}
			{{ Form::number('phone', $value=old('phone') ,array('class' => 'form-control','placeholder' => 'Phone Number')) }}
			@if(count($errors) > 0)
                @foreach($errors->get('phone') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
           	</div>
        	</div>
        	</div>
			<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
	    	{{ Form::label('Gender :','',array('class' => 'font-weight-bold')) }}
			{{ Form::label('Male','',array('class' => 'font-weight-bold text-primary')) }} 
			{{ Form::radio('gender', 'male') }}
			&nbsp;&nbsp;
  			{{ Form::label('Female','',array('class' => 'font-weight-bold text-primary')) }}
  			{{ Form::radio('gender', 'female') }}

  			@if(count($errors) > 0)
                @foreach($errors->get('gender') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
            </div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="">
  			{{ Form::label('Profile Photo :','',array('class' => 'font-weight-bold')) }}
			{{ Form::file('image')  }}
			@if(count($errors) > 0)
                @foreach($errors->get('image') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
            </div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
			{{ Form::label('Email :','',array('class' => 'font-weight-bold')) }}
			{{ Form::email('email', $value=old('email'),array('class' => 'form-control','placeholder' => 'Email')) }}
			@if(count($errors) > 0)
                @foreach($errors->get('email') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
            </div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
			{{ Form::label('Status :','',array('class' => 'font-weight-bold')) }}
			{{ Form::label('Active','',array('class' => 'font-weight-bold text-primary')) }}
			{{ Form::radio('status', '1' ) }}
			&nbsp;&nbsp;
  			{{ Form::label('inactive','',array('class' => 'font-weight-bold text-primary')) }}
  			{{ Form::radio('status', '0' ) }}

  			@if(count($errors) > 0)
                @foreach($errors->get('status') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
            </div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
  			{{ Form::label('Password :','',array('class' => 'font-weight-bold ')) }}
			{{Form::password("Password", 
             [
                "class" => "form-control",
                "placeholder" => "Your Password",
             ])
			}}
			@if(count($errors) > 0)
                @foreach($errors->get('Password') as $er)
                    <p class="text-danger font-weight-bold mt-3">{{$er}}</p>
                @endforeach
            @endif
            </div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
			{{ Form::label('Confirm xPassword :','',array('class' => 'font-weight-bold ')) }}
			{{Form::password("Password_confirmation", 
             [
                "class" => "form-control",
                "placeholder" => "Confirm Password",
             ])
			}}
			</div>
        	</div>
        	</div>
			<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
			{{ Form::submit('Submit',array('class' => 'btn btn-primary btn-block text-light')) }}
			</div>
        	</div>
        	</div>
		{{ Form::close() }}
		</div>
       	</div>
    </div>
    </div>
</div>
</body>
@endsection