@extends('admin.layout.master')
@section('title','Dashboard')
@section('section')

    <h1>Login Form</h1>
 <div class="content">
    <div class="row">
    	<div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> User List</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Image</th> 
                      <th>Name</th>
                      <th>Phone</th>
                      <th>Gender</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Edit</th>
                      <th>Delete</th>
                    </thead>
                    <tbody>
                    @foreach ($data as $data)
                     <tr>
                        <td><img  width=100px src="img/user_image/{{$data->image}}"></td>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->phone }}</td>
                        <td>{{ $data->gender }}</td>
                        <td>{{ $data->email }}</td>
                        <td>
                        	@if($data->status == 1) 
                         		<button type="button"  class="btn btn-primary"><a href="/status_change/{{$data->id}}">Active</a></button> 
                     		@else
                      			<button type="button" href="/status_change/{{$data->id}}" class="btn btn-danger"><a href="/status_change/{{$data->id}}" >Inactive</a></button>
                  			@endif
                  		 </td>
                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{ $data->id }}">Edit</button></td>
                         <td><a href="/delete_user/{{ $data->id }}" ><button type="button" class="btn btn-primary" >Delete</button></a></td>
                      </tr>



                      <!-- The Modal -->
  <div class="modal" id="myModal{{ $data->id }}">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         {{ Form::open(array('url' => '/update_user','files'=>'true')) }}

         {{ Form::hidden('id',  $data->id ) }}

         <img  width=100px src="img/user_image/{{$data->image}}"><br>

        {{ Form::label('Name :*','',array('class' => 'font-weight-bold')) }}
		{{ Form::text('name',  $data->name ,array('class' => 'form-control-plaintext border border-danger')) }}<br>

		{{ Form::label('Phone Number :*','',array('class' => 'font-weight-bold')) }}
		{{ Form::number('phone', $data->phone ,array('class' => 'form-control-plaintext border border-danger')) }}<br>

		{{ Form::label('Gender :*','',array('class' => 'font-weight-bold')) }}
		{{ Form::label('Male','',array('class' => 'font-weight-bold text-primary')) }} 
		{{ Form::radio('gender', 'male') }}
		&nbsp;&nbsp;
		{{ Form::label('Female','',array('class' => 'font-weight-bold text-primary')) }}
		{{ Form::radio('gender', 'female') }}<br><br>

		{{ Form::label('Profile Photo :*','',array('class' => 'font-weight-bold')) }}
		{{ Form::file('userImage', array('multiple'=>false,	 'class' => 'font-weight-bold text-primary'))  }}<br><br>

		{{ Form::label('Email :','',array('class' => 'font-weight-bold')) }}
		{{ Form::email('email', $data->email ,array('class' => 'form-control-plaintext border border-danger')) }}

		{{ Form::submit('Submit',array('class' => 'btn btn-primary .text-light')) }}

        </div>
       
         {{ Form::close() }}
        
      </div>
    </div>
  </div>


                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
         </div>
    </div>
</div>

@endsection