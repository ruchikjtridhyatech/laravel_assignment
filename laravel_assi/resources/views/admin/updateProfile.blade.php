@extends('admin.layout.master')
@section('title','Update Profile')
@section('section')
<body class="user-profile">
<div class="content">
    <div class="row">
    <div class="col-md-8">
    	<div class="card">
	    <div class="card-header">
	        <h5 class="title">Edit Profile</h5>
	    </div>
        <div class="card-body">

            
		{{ Form::open(array('url' => '/update_admin','files'=>'true')) }}
            <img  width=100px src="img/user_image/">
            <div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
	    	{{ Form::label('Name :','',array('class' => 'font-weight-bold')) }}
			{{ Form::text('name', $data['name'],array('class' => 'form-control','placeholder' => 'Full Name')) }}
			
        	</div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
			{{ Form::label('Phone Number :','',array('class' => 'font-weight-bold')) }}
			{{ Form::number('phone','' ,array('class' => 'form-control','placeholder' => 'Phone Number')) }}
			
           	</div>
        	</div>
        	</div>
			<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
	    	{{ Form::label('Gender :','',array('class' => 'font-weight-bold')) }}
			{{ Form::label('Male','',array('class' => 'font-weight-bold text-primary')) }} 
			{{ Form::radio('gender', 'male') }}
			&nbsp;&nbsp;
  			{{ Form::label('Female','',array('class' => 'font-weight-bold text-primary')) }}
  			{{ Form::radio('gender', 'female') }}

  			
            </div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="">
  			{{ Form::label('Profile Photo :','',array('class' => 'font-weight-bold')) }}
			{{ Form::file('image')  }}
			
            </div>
        	</div>
        	</div>
        	<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
			{{ Form::label('Email :','',array('class' => 'font-weight-bold')) }}
			{{ Form::email('email', '',array('class' => 'form-control','placeholder' => 'Email')) }}
			
            </div>
        	</div>
        	</div>
        	
			<div class="row">
            <div class="col-md-12 pr-1">
            <div class="form-group">
			{{ Form::submit('Submit',array('class' => 'btn btn-primary btn-block text-light')) }}
			</div>
        	</div>
        	</div>
		{{ Form::close() }}
	
		</div>
       	</div>
    </div>
    </div>
</div>
</body>
@endsection