@extends('admin.layout.master')
@section('title','Login')
@section('section')
<center>
<h1 class="text-dark">Admin Login</h1>
{{ Form::open(array('url' => '/admin_login')) }}
	<table cellpadding="10" class="w-50 ">
	
	
	<tr>
    	<td>{{ Form::label('Email :','',array('class' => 'font-weight-bold')) }}</td>
		<td>{{ Form::email('email', $value=old('email'),array('class' => 'form-control-plaintext border border-danger','placeholder' => 'Email')) }}
		</td>
	</tr>
	<tr>
		<td>{{ Form::label('Password :','',array('class' => 'font-weight-bold ')) }}</td>
		<td>{{Form::password("Password", 
             [
                "class" => "form-control-plaintext border border-danger",
                "placeholder" => "Your Password",
             ])
			}}
			
		</td>
	</tr>
	<tr>
		<td colspan="2" class="text-center">{{ Form::submit('Submit',array('class' => 'btn btn-primary .text-light')) }}</td>
	</tr>
	</table>
	{{ Form::close() }}
</center>
@endsection